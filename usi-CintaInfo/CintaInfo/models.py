from cms.models.pluginmodel import CMSPlugin
from django.db import models
from cropperjs.models import CropperImageField

# Create your models here.
class CintaInfo(CMSPlugin):
    titulo = models.CharField(blank=True, max_length=30)
    texto = models.CharField(blank=True, max_length=200)
    
class btnCinta(CMSPlugin):
    texto = models.CharField(null=False, max_length=20)
    url = models.URLField(null=False)

