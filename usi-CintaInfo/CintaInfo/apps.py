from django.apps import AppConfig


class CintainfoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CintaInfo'
