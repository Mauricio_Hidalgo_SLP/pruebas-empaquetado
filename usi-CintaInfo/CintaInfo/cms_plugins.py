from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
# Importamos los modelos de models.py
from .models import CintaInfo, btnCinta

class CintaInfoPlugin(CMSPluginBase):
    render_template = 'Cinta_Parent.html'
    name = 'Cinta con Información'
    model = CintaInfo
    allow_children = True 
    child_classes = ['contentBtn']

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(CintaInfoPlugin)


class contentBtn(CMSPluginBase):
    render_template = 'CintaBtn_Child.html'
    name = 'Botón'
    model = btnCinta
    require_parent = True

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(contentBtn)
