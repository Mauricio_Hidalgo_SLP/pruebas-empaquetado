=====
Cinta Informativa para Django.
=====

Cinta Informativa para django.

-----------
1. Run command `pip install -e MyProject @ gitrepo//usi-CintaInfo/...`

2. Add "CintaInfo" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'CintaInfo.apps.CintainfoConfig',
',
    )

3. Include the polls URLconf in your project urls.py like this::

    url(r'^polls/', include('polls.urls')),

4. Run `python manage.py migrate` to create the polls models.
