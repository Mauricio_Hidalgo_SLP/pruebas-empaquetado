from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
# Importamos los modelos de models.py
from .models import VidDescr

class VideoDescrPlugin(CMSPluginBase):
    render_template = 'VideoDesc.html'
    name = 'Video o imagen con descripción'
    model = VidDescr
    allow_children = False 

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(VideoDescrPlugin)
