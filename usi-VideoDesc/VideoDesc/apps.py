from django.apps import AppConfig


class VideodescConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'VideoDesc'
