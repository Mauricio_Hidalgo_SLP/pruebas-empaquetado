from cms.models.pluginmodel import CMSPlugin
from django.db import models
from cropperjs.models import CropperImageField

# Create your models here.
class VidDescr(CMSPlugin):
    titulo = models.CharField(null=False, max_length=50)
    texto = models.CharField(null=False, max_length=500)
    url = models.URLField(blank=True, verbose_name="URL del video")
    imagen = CropperImageField(null=False, upload_to='static/img', dimensions=(620, 520), verbose_name="Imagen de portada del video")
    
    @property
    def parseUrl(self):
        return self.url.replace('watch?v=', 'embed/')