# Generated by Django 4.2.5 on 2023-10-02 18:54

import cropperjs.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('VideoDesc', '0003_remove_viddescr_imagen_alter_viddescr_descripcion_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='viddescr',
            name='descripcion',
        ),
        migrations.AddField(
            model_name='viddescr',
            name='imagen',
            field=cropperjs.models.CropperImageField(aspectratio='1.1923076923076923', default='ERR', dimensions=(620, 520), upload_to='static/img', verbose_name='Imagen de portada del video'),
            preserve_default=False,
        ),
    ]
