=====
Video o Imagen con Descripcion para Django.
=====

Video o Imagen con Descripcion para django.

-----------
1. Run command `pip install -e MyProject @ gitrepo//usi-VideoDesc/...`

2. Add "VideoDesc" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'VideoDesc.apps.VideodescConfig',
',
    )

3. Include the polls URLconf in your project urls.py like this::

    url(r'^polls/', include('polls.urls')),

4. Run `python manage.py migrate` to create the polls models.
