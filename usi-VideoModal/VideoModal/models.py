from cms.models.pluginmodel import CMSPlugin
from django.db import models
from cropperjs.models import CropperImageField

# Create your models here.
class VidModal(CMSPlugin):
    url = models.URLField(null=False, verbose_name="URL del video")
    imagen = CropperImageField(null=False, upload_to='static/img', dimensions=(920, 520), verbose_name="Imagen de portada del video")
    # subtitulo = models.CharField(max_length=200)

    @property
    def parseUrl(self):
        return self.url.replace('watch?v=', 'embed/')