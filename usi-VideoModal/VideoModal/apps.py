from django.apps import AppConfig


class VideomodalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'VideoModal'
