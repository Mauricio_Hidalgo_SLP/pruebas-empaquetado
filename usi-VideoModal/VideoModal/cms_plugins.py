from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
# Importamos los modelos de models.py
from .models import VidModal

class VideoModalPlugin(CMSPluginBase):
    render_template = 'VideoModal.html'
    name = 'Video'
    model = VidModal
    allow_children = False 

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(VideoModalPlugin)
