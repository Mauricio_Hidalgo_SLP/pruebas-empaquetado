=====
Video sobre Modal para Django.
=====

Video sobre Modal para django.

-----------
1. Run command `pip install -e MyProject @ gitrepo//usi-VideoModal/...`

2. Add "VideoModal" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'VideoModal.apps.VideomodalConfig',
',
    )

3. Include the polls URLconf in your project urls.py like this::

    url(r'^polls/', include('polls.urls')),

4. Run `python manage.py migrate` to create the polls models.
