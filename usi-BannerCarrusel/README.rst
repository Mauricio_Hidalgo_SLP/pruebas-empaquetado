=====
Banner con Carrusel para Django.
=====

Banner con Carrusel para django.

-----------
1. Run command `pip install -e MyProject @ gitrepo//usi-BannerCarrusel/...`

2. Add "BannerCarrusel" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'BannerCarrusel.apps.BannercarruselConfig',
',
    )

3. Include the polls URLconf in your project urls.py like this::

    url(r'^polls/', include('polls.urls')),

4. Run `python manage.py migrate` to create the polls models.
