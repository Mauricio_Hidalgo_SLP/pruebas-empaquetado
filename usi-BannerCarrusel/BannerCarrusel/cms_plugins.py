from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import gettext_lazy
# Importamos los modelos de models.py
from .models import Banner, ImgButton, ImgOnly, Buttons

class HelloPlugin(CMSPluginBase):
    render_template = 'banner_parent.html'
    name = 'Cuadro para Carrusel'
    # model = Banner
    allow_children = True 
    child_classes = ['Child2CMSPlugin', 'ChildCMSPlugin']

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(HelloPlugin)


class ChildCMSPlugin(CMSPluginBase):
    render_template = 'carousel_child.html'
    name = 'Imagen con Contenido'
    model = ImgButton
    require_parent = True
    allow_children = True
    child_classes = ['ChildBTNCMSPlugin']
    
    # Datos extras a la hora de renderizar!
    def render(self, context, instance, placeholder):
        context = super().render(context, instance, placeholder)
        
        # Manda la variable otorgada a allow_children y la coloca en una nueva variable, luego la agrega al contexto de renderizado.
        context['allow_children'] = self.allow_children
        return context

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(ChildCMSPlugin)

class Child2CMSPlugin(CMSPluginBase):
    render_template = 'carousel_child.html'
    name = 'Imagen'
    model = ImgOnly
    require_parent = True
       
# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(Child2CMSPlugin)

class ChildBTNCMSPlugin(CMSPluginBase):
    render_template = 'btn_child_child.html'
    name = 'Botón'
    model = Buttons
    require_parent = True
    
plugin_pool.register_plugin(ChildBTNCMSPlugin)
