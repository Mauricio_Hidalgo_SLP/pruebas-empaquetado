from cms.models.pluginmodel import CMSPlugin
from django.db import models
from cropperjs.models import CropperImageField
from colorfield.fields import ColorField


# Create your models here.
class Banner(CMSPlugin):
    titulo = models.CharField(max_length=20)
    # subtitulo = models.CharField(max_length=200)
    
class ImgButton(CMSPlugin):
    titulo = models.CharField(null=True, max_length=50)
    subtitulo = models.CharField(blank=True, max_length=50)
    imagen_Escritorio = CropperImageField(null=False, upload_to='static/img',  dimensions=(1900, 1900))
    # imagen_Movil = CropperImageField(null=False, upload_to = 'static/img', dimensions=(1900, 1900))   
    # Array de colores a escojer
    color_choices =[
    ("#191F23", "Oscuro 1"),
    ("#055A1C", "Verde 1"),
    ("#338C36", "Verde 2"),
    ("#76B82A", "Verde 3"),
    ("#E60064", "Rosa 1")
    ]
    # Para abrir las opciones cambiar "choices" por "samples"
    color = ColorField(default="#191F23", choices=color_choices)

class ImgOnly(CMSPlugin):
    titulo = models.CharField(blank=True, max_length=50)
    subtitulo = models.CharField(blank=True, max_length=100)
    imagen_Escritorio = CropperImageField(null=False, upload_to='static/img',  dimensions=(1900, 400))
    imagen_Movil = CropperImageField(null=False, upload_to = 'static/img', dimensions=(850, 1900))
    color_choices = [
        ("#FFFFFF", "Claro 1"),
        ("#191F23", "Oscuro 1"),
        ("#055A1C", "Verde 1"),
        ("#338C36", "Verde 2"),
        ("#76B82A", "Verde 3"),
        ("#E60064", "Rosa 1")
    ]
    # Para abrir las opciones cambiar "choices" por "samples"
    color = ColorField(default="#191F23", choices=color_choices)
    
class Buttons(CMSPlugin):
    texto = models.CharField(null=False, max_length=20)
    url = models.URLField(null=False)
