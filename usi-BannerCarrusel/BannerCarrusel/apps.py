from django.apps import AppConfig


class BannercarruselConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'BannerCarrusel'
