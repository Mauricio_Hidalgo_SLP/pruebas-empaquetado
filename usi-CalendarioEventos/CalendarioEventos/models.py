from cms.models.pluginmodel import CMSPlugin
from django.db import models
from cropperjs.models import CropperImageField


# Create your models here.
class Calendario(CMSPlugin):
    titulo = models.CharField(blank=True, max_length=50)
    
class Evento(CMSPlugin):
    titulo = models.CharField(null=False, max_length=30)
    texto = models.CharField(blank=True, max_length=200)
    fecha_inicio = models.DateField(null=False)
    fecha_fin = models.DateField(blank=True, null=True)
    hora_inicio = models.TimeField(null=False)
    hora_fin = models.TimeField(blank= True)
    direccion = models.CharField(null=False, max_length=50)
    latitud = models.DecimalField(null=False, max_digits=12, decimal_places=6)
    longitud = models.DecimalField(null=False, max_digits=12, decimal_places=6)
    imagen = CropperImageField(blank=True, upload_to='static/img', dimensions=(420, 420))
    
    # Funciones para generar las URL de los mapas:
    def url_movil(self):
        return  'geo:'+ str(self.latitud) + ', ' + str(self.longitud)
    
    def url_pc(self):
        return  'http://www.google.com/maps/place/'+ str(self.latitud) + ', ' + str(self.longitud)
    
    # Funciones para formatear la Fecha:
    def formatFechaInicio(self):
        return self.fecha_inicio.strftime("%a %d de %b, %Y")
    
    def formatFechaFin(self):
        return self.fecha_fin.strftime("%a %d de %b, %Y")
    
    # # Funciones para formatear la Hora:
    # def formatHoraInicio(self):
    #     return self.hora_inicio.strftime("")
    
    # def formatHoraFin(self):
    #     return self.hora_fin.strftime("")
        