# Generated by Django 4.2.5 on 2023-10-04 21:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CalendarioEventos', '0006_alter_evento_direccion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evento',
            name='direccion',
            field=models.CharField(max_length=50),
        ),
    ]
