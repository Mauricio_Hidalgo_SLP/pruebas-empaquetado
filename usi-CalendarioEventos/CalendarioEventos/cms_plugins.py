from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
# Importamos los modelos de models.py
from .models import Calendario, Evento

class CalendarioPlugin(CMSPluginBase):
    render_template = 'Calendario_Parent.html'
    name = 'Calendario de eventos'
    # model = Calendario
    allow_children = True 
    child_classes = ['contentEvento']

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(CalendarioPlugin)


class contentEvento(CMSPluginBase):
    render_template = 'Calendario_Child.html'
    name = 'Evento'
    model = Evento
    require_parent = True

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(contentEvento)
