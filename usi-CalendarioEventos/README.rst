=====
Calendario de Eventos para Django.
=====

Calendario de Eventos para django.

-----------
1. Run command `pip install -e MyProject @ gitrepo//usi-CalendarioEventos/...`

2. Add "CalendarioEventos" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'CalendarioEventos.apps.CalendarioeventosConfig',
',
    )

3. Include the polls URLconf in your project urls.py like this::

    url(r'^polls/', include('polls.urls')),

4. Run `python manage.py migrate` to create the polls models.
