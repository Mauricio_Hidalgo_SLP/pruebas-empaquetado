from django.apps import AppConfig


class ImginfoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ImgInfo'
