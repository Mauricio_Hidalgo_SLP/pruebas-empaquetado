# Generated by Django 4.2.5 on 2023-09-27 20:05

import colorfield.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ImgInfo', '0009_alter_divinfoimg_color'),
    ]

    operations = [
        migrations.AlterField(
            model_name='divinfoimg',
            name='color',
            field=colorfield.fields.ColorField(choices=[('#FFFFFF', 'Sin color'), ('#055A1C', 'Verde 1'), ('#338C36', 'Verde 2'), ('#76B82A', 'Verde 3'), ('#E60064', 'Rosa 1')], default='', image_field=None, max_length=25, samples=None),
        ),
    ]
