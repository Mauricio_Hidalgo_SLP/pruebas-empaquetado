from django.forms import ModelForm, Select
from .models import DivInfoImg

# Clase para modificar el formulario del modelo DIvInfoImg
class DivInfoImg_frm(ModelForm):
    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
        
    #     for choice in self.fields["color"].choices: 
    #         choice[1]['attrs']['style'] = 'color: #ffffff'

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['color'].widget.choices[0] = ("style='color: #ffffff;'", "Sin Color")
    #     self.fields['color'].widget.choices[1] = ("", "<span style='color: #055A1C;'>nombre</span>")
    #     self.fields['color'].widget.choices[2] = ("", "<span style='color: #338C36;'>nombre</span>")
    #     self.fields['color'].widget.choices[3] = ("", "<span style='color: #76B82A;'>nombre</span>")
    #     self.fields['color'].widget.choices[4] = ("", "<span style='color: #E60064;'>nombre</span>")
        
    class Meta:
        model = DivInfoImg
        fields = "__all__"
        widgets = {
            'color': Select(attrs={ 'style': 'color: red;', }), 
            }        
        