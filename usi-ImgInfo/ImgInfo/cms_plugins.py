from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import gettext_lazy
# Importamos los modelos de models.py
from .models import ContentBtn, DivInfoImg
# Importamos los formularios personalizados!
from .forms import DivInfoImg_frm

class InfoImagenPlugin(CMSPluginBase):
    render_template = 'InfoImg_Parent.html'
    name = 'Fondo con Información'
    model = DivInfoImg
    allow_children = True 
    child_classes = ['infoImgBtnPlugin']
    
    form = DivInfoImg_frm

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(InfoImagenPlugin)


class infoImgBtnPlugin(CMSPluginBase):
    render_template = 'btn_child_infoImg.html'
    name = 'Botón'
    model = ContentBtn
    require_parent = True

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(infoImgBtnPlugin)

# def render(self, context, instance, placeholder):
#     context = super(ChildCMSPlugin, self).render(context, instance, placeholder)
#     return context
