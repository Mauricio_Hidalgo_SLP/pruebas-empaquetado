from cms.models.pluginmodel import CMSPlugin
from django.db import models
from colorfield.fields import ColorField
from cropperjs.models import CropperImageField

# Create your models here.
class DivInfoImg(CMSPlugin):
    titulo = models.CharField(null=False, max_length=50, default="Fondo con Información")
    texto = models.CharField(blank=True, max_length=200)
    imagen = CropperImageField(blank=True, upload_to='static/img', dimensions=(1900, 840))
    # Array de colores a escojer
    color_choices =[
    ("#FFFFFF", "Sin color"),
    ("#055A1C", "Verde 1"),
    ("#338C36", "Verde 2"),
    ("#76B82A", "Verde 3"),
    ("#E60064", "Rosa 1")
    ]
    # Para abrir las opciones cambiar "choices" por "samples"
    color = ColorField(default="#FFFFFF", choices=color_choices)
    
class ContentBtn(CMSPlugin):
    texto = models.CharField(null=False, max_length=1000)
    url = models.URLField(null=False)