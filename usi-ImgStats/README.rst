=====
Imagen con Estadisticas para Django.
=====

Imagen con Estadisticas para django.

-----------
1. Run command `pip install -e MyProject @ gitrepo//usi-ImgStats/...`

2. Add "ImgStats" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'ImgStats.apps.ImgstatsConfig',
',
    )

3. Include the polls URLconf in your project urls.py like this::

    url(r'^polls/', include('polls.urls')),

4. Run `python manage.py migrate` to create the polls models.
