from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
# Importamos los modelos de models.py
from .models import ImageStats, StatsBar


class ImagenStatsPlugin(CMSPluginBase):
    render_template = 'parent_ImgStats.html'
    name = 'Imagen y estadísticas'
    model = ImageStats
    allow_children = True 
    child_classes = ['contentStats']

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(ImagenStatsPlugin)

class contentStats(CMSPluginBase):
    render_template = 'child_Stats.html'
    name = 'Barra con porcentaje'
    model = StatsBar
    require_parent = True

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(contentStats)