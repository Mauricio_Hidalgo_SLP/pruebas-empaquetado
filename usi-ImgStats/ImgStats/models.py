from cms.models.pluginmodel import CMSPlugin
from django.db import models
from cropperjs.models import CropperImageField

# Create your models here.
class ImageStats(CMSPlugin):
    titulo = models.CharField(null=False, max_length=50, default="Fondo con Estadísticas")
    texto = models.CharField(blank=True, max_length=200)
    imagen = CropperImageField(blank=True, upload_to='static/img', dimensions=(480, 680))
    
class StatsBar(CMSPlugin):
    encabezado = models.CharField(blank=True, max_length=30)
    num_barra = models.IntegerField(verbose_name="Porcentaje de Barra: (0 a 100)", default=100)
