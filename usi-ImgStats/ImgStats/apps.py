from django.apps import AppConfig


class ImgstatsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ImgStats'
