=====
Linea de Tiempo para Django.
=====

Linea de Tiempo para django.

-----------
1. Run command `pip install -e MyProject @ gitrepo//usi-LineaTiempo/...`

2. Add "LineaTiempo" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'LineaTiempo.apps.LineatiempoConfig',
',
    )

3. Include the polls URLconf in your project urls.py like this::

    url(r'^polls/', include('polls.urls')),

4. Run `python manage.py migrate` to create the polls models.
