from cms.models.pluginmodel import CMSPlugin
from django.db import models
from cropperjs.models import CropperImageField

# Create your models here.
class DivTimeLine(CMSPlugin):
    titulo = models.CharField(null=False, max_length=20, default="Linea de Tiempo")
    # subtitulo = models.CharField(max_length=200)
    
class ContentImg(CMSPlugin):
    titulo = models.CharField(blank=False, max_length=20, default='Titulo')
    subtitulo = models.CharField(blank=False, max_length=20, default='Subtitulo')
    texto = models.CharField(blank=True, max_length=500)
    imagen = CropperImageField(blank=True, upload_to='static/img', dimensions=(960, 960))
