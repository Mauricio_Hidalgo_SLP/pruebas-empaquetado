from django.apps import AppConfig


class LineatiempoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'LineaTiempo'
