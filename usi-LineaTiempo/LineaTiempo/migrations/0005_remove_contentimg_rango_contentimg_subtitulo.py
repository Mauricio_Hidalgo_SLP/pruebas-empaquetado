# Generated by Django 4.2.5 on 2023-09-25 17:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('LineaTiempo', '0004_contentimg_titulo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contentimg',
            name='rango',
        ),
        migrations.AddField(
            model_name='contentimg',
            name='subtitulo',
            field=models.CharField(default='Subtitulo', max_length=20),
            preserve_default=False,
        ),
    ]
