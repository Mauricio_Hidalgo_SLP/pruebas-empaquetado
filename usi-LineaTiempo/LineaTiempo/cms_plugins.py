from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import gettext_lazy
# Importamos los modelos de models.py
from .models import ContentImg, DivTimeLine

class TimeLinePlugin(CMSPluginBase):
    render_template = 'TimeLine_Parent.html'
    name = 'Linea de Tiempo'
    # model = DivTimeLine
    allow_children = True 
    child_classes = ['Content1_TL']

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(TimeLinePlugin)


class Content1_TL(CMSPluginBase):
    render_template = 'TL_Child.html'
    name = 'Contenido de Linea de Tiempo'
    model = ContentImg
    require_parent = True

# Se registra el PlugIn en la lista de Plugins!
plugin_pool.register_plugin(Content1_TL)

# def render(self, context, instance, placeholder):
#     context = super(ChildCMSPlugin, self).render(context, instance, placeholder)
#     return context
